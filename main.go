package main

import (
	"crypto/sha256"
	"crypto/tls"
	_ "embed"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	u "github.com/bcicen/go-units"
	irc "github.com/fluffle/goirc/client"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
	_ "github.com/joho/godotenv/autoload"
	"golang.org/x/net/html"
	"math/rand"
	"strconv"
	"time"

	crypto_rand "crypto/rand"
	"log"
	math_rand "math/rand"
	"net/http"
	"os"
	"strings"
)

//go:embed doc.md
var doc string

var JIRC_URL = os.Getenv("JIRC_URL")

func privmsgRateLimited(conn *irc.Conn, channel string, msgs []string) {
	commandRate, err := strconv.Atoi(os.Getenv("JIRC_COMMAND_RATE"))
	if err != nil {
		commandRate = 1000
	}
	waitFor := time.Duration(commandRate) * time.Millisecond

	for _, msg := range msgs {
		time.Sleep(waitFor)
		conn.Privmsg(channel, msg)

	}
}

func randInt(min int, max int) int {

	var b [8]byte
	_, err := crypto_rand.Read(b[:])
	if err != nil {
		panic("cannot seed math/rand package with cryptographically secure random number generator")
	}
	math_rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
	return min + math_rand.Intn(max-min)
}

func yc() []string {
	link := "https://news.ycombinator.com/front?day=" + time.Now().AddDate(0, 0, -1).Format("2006-01-02")

	var msgs []string
	resp, err := http.Get(link)
	if err != nil {
		// handle error
	}
	defer resp.Body.Close()
	doc, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			attrMap := make(map[string]string)

			for _, a := range n.Attr {
				attrMap[a.Key] = a.Val
			}

			if val, ok := attrMap["class"]; ok {
				//do something here
				if val == "storylink" {
					msgs = append(msgs, attrMap["href"])
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	// end build messages
	return msgs
}

func convert(conn *irc.Conn, channel string, text string) {
	if text == ",convert" || text == ",convert help" {
		privmsgRateLimited(conn, channel, []string{JIRC_URL + "/#convert"})
		return
	}
	vals := strings.Split(text, " ")
	vals = vals[1:len(vals)]
	from, err := u.Find(vals[1])
	if err != nil {
		privmsgRateLimited(conn, channel, []string{vals[1] + " not valid unit"})
		return
	}
	to, err := u.Find(vals[2])
	if err != nil {
		privmsgRateLimited(conn, channel, []string{vals[2] + " not valid unit"})
		return
	}

	val, err := strconv.ParseFloat(vals[0], 64)
	if err != nil {
		privmsgRateLimited(conn, channel, []string{vals[0] + " failed to parse"})
		return
	}

	message, err := u.ConvertFloat(val, from, to)

	if err != nil {
		privmsgRateLimited(conn, channel, []string{"failed to preform conversion for: " + vals[0] + " " + vals[1] + " " + " " + vals[2]})
		return

	}
	privmsgRateLimited(conn, channel, []string{fmt.Sprintf("%s %ss is %s", vals[0], from.Name, message.String())})
	return
}

func yt(text string) []string {
	if text == "" || text == "help" {
		return []string{JIRC_URL + "/#yt"}

	}
	// TODO: would be nice to handle https://www.youtube.com/c/STLChessClub/videos
	// style links too
	if strings.Contains(text, "playlist") {
		return []string{fmt.Sprintf(
			",feed add %s",
			"https://www.youtube.com/feeds/videos.xml?playlist_id="+text[38:],
		)}
	} else if strings.Contains(text, "channel") {
		splitText := strings.Split(text, "/")
		ytChannelID := splitText[len(splitText)-1]
		return []string{fmt.Sprintf(
			",feed add %s",
			"https://www.youtube.com/feeds/videos.xml?channel_id="+ytChannelID,
		)}

	} else {
		return []string{fmt.Sprintf("url format not recognised for %s", text)}
	}
}

func ttv(text string) []string {
	if text == "" || text == "help" {
		return []string{JIRC_URL + "/#ttv"}

	}
	splitText := strings.Split(text, "/")
	twitchChannelID := splitText[len(splitText)-1]
	return []string{fmt.Sprintf("/feed add https://twitchrss.appspot.com/vod/%s", twitchChannelID)}

}

func roll(text string) []string {

	if text == "" || text == "help" {
		return []string{JIRC_URL + "/#roll"}

	}

	i, err := strconv.Atoi(text)
	if err != nil {
		return []string{"Invalid input " + text}
	}
	if i <= 0 {
		return []string{"provide integer greater than 0"}
	}
	rand.Seed(time.Now().UnixNano())

	randInt := randInt(1, i)
	msg := strconv.Itoa(randInt)
	return []string{msg}

}

func choose(text string) []string {
	if text == "" || text == "help" {
		return []string{JIRC_URL + "/#choose"}

	}
	vals := strings.Split(text, " ")
	var msg string
	if len(vals) > 0 {
		msg = vals[randInt(0, len(vals)-1)]
	} else {
		msg = "nothing to choose from"
	}
	return []string{msg}
}

func mysha256(text string) []string {
	if text == "" || text == "help" {
		return []string{JIRC_URL + "/#sha256"}

	}
	h := sha256.New()
	h.Write([]byte(text))
	msg := hex.EncodeToString(h.Sum(nil))
	return []string{msg}

}

func main() {
	// send messages to irc server
	// Or, create a config and fiddle with it first:
	cfg := irc.NewConfig(os.Getenv("JIRC_NICK"))
	cfg.SSL = true
	cfg.SSLConfig = &tls.Config{ServerName: os.Getenv("JIRC_SERVER")[0 : len(os.Getenv("JIRC_SERVER"))-5]}
	cfg.Server = os.Getenv("JIRC_SERVER")
	cfg.NewNick = func(n string) string { return n + "^" }
	cfg.Pass = os.Getenv("JIRC_PASS")
	c := irc.Client(cfg)

	// Add handlers to do things here!
	// e.g. join a channel on connect.
	c.HandleFunc(irc.CONNECTED,
		func(conn *irc.Conn, line *irc.Line) {
			channels := strings.Split(os.Getenv("JIRC_CHANS"), ",")
			for _, c := range channels {
				conn.Join("#" + c)
			}
		})
	// And a signal on disconnect
	quit := make(chan bool)
	c.HandleFunc(irc.DISCONNECTED,
		func(conn *irc.Conn, line *irc.Line) { quit <- true })

	c.HandleFunc(irc.INVITE, func(conn *irc.Conn, line *irc.Line) {
		for index, arg := range line.Args {
			if index > 0 {
				conn.Join(arg)
			}
		}
	})

	c.HandleFunc(irc.PRIVMSG, func(conn *irc.Conn, line *irc.Line) {
		channel := line.Args[0]
		prefix := strings.Split(line.Args[1], " ")[0]
		rest := line.Args[1][len(prefix):]
		if len(rest) > 0 && rest[0] == ' '{
			rest = rest[1:]
		}
		switch prefix {
		case ",echo":
			conn.Privmsg(channel, rest[0:])
		case ",yc":
			privmsgRateLimited(conn, channel, yc())
		case ",convert":
			convert(conn, channel, rest)
		case ",yt":
			privmsgRateLimited(conn, channel, yt(rest))
		case ",ttv":
			privmsgRateLimited(conn, channel, ttv(rest))
		case ",roll":
			privmsgRateLimited(conn, channel, roll(rest))
		case ",choose":
			privmsgRateLimited(conn, channel, choose(rest))
		case ",sha256":
			privmsgRateLimited(conn, channel, mysha256(rest))

		}
	})

	// Tell client to connect.
	if err := c.Connect(); err != nil {
		fmt.Printf("Connection error: %s\n", err.Error())
	}

	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("OK"))
		if err != nil {
			log.Println(err)
		}
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.Tables
		parser := parser.NewWithExtensions(extensions)

		md := []byte(doc)
		html := markdown.ToHTML(md, parser, nil)
		_, err := w.Write(html)

		if err != nil {
			log.Println(err)
		}
	})
	log.Println("server listening")
	http.ListenAndServe(":8000", nil)
}
