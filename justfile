tidy:
    go mod tidy

build: tidy
     docker build -t jirc:latest .

run: build
    docker run -p 8000:8000 --env-file .env jirc:latest