module jirc

go 1.16

require (
	github.com/bcicen/go-units v1.0.2
	github.com/fluffle/goirc v1.1.1
	github.com/gomarkdown/markdown v0.0.0-20210820032736-385812cbea76
	github.com/joho/godotenv v1.3.0
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
)
