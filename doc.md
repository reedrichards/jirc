# JIRC

[source code](https://gitlab.com/reedrichards/jirc)

jirc (pronounced jerk) is an irc bot that supports various utility commands. 

## Quickstart

If you have [just](https://github.com/casey/just) and [docker](https://docs.docker.com/get-docker/) installed, you can
start the project with `just run`.

```shell
git clone https://gitlab.com/reedrichards/jirc
cd jirc
just run
```

## Setup and Configuration

`jirc` is written in [go](https://golang.org/) version 1.16 and 
distributed via [docker](https://www.docker.com/).

### Build

#### Docker
- ensure you have [docker](https://docs.docker.com/get-docker/) installed.
- clone the repo and build the docker image 

```shell
git clone https://gitlab.com/reedrichards/jirc
cd jirc
docker build -t jirc:latest .
```

- create a `.env` file and configure [environment variables](#environment-variables)
- run the server with

```shell
docker run -p 8000:8000 --env-file .env jirc:latest
```

#### Building From Source

- Ensure you have [go 1.16](https://golang.org/dl/) installed 
- clone the repo and change into the directory

```shell
git clone https://gitlab.com/reedrichards/jirc
cd jirc
```
- 
- build the project
```shell
go build
```
- create a `.env` file and configure [environment variables](#environment-variables)
- run the bot 

```shell
./jirc
```

### Environment Variables:

| variable | required | example | description |
|---------|-----------|----|---------------|
| JIRC_NICK  | YES | jirc      | the nick to be used by jirc |
| JIRC_SERVER  | YES | smelly.irc.jjk.is:6697 | server and port that jirc will use to connect |
| JIRC_PASS  | YES | soopersekret      | password to use when connecting |
| JIRC_CHANS | YES | chan1,chan2,chan3      | comma seperated list of channels to join at startup|
| JIRC_COMMAND_RATE  | NO | 1000  | command rate limit in milliseconds, defaults to `1000` (one second) |
| JIRC_URL  | YES | http://localhost:8000  | url which webserver is being served |


## Commands and Usage

in order to run these commands you *must* invite `jirc` to your channel. You can accomplish this by running `/INVITE jirc` 
in the channel that you wish `jirc` to join, or `/INVITE  jirc #chan1 #chan2` to invite to multiple channels. `jirc` only supports joining public channels. 

### echo

says whatever you said back to you

exampe usage:
```text
<foo> echo foo bar baz
<jirc> echo foo bar baz
```

### ,yc

Fetch links from yesterdays front page of [hackernews](https://news.ycombinator.com/front) and post them to the channel.

example usage:
```text
<foo> ,yc
<jirc> https://www.smithsonianmag.com/smart-news/leaded-gas-poison-invented-180961368/
<jirc> https://pipewire.org/
<jirc> https://my-room-in-3d.vercel.app/
<jirc> https://twitter.com/pixelscript/status/1436664488913215490
<jirc> https://bottosson.github.io/posts/colorpicker/
<jirc> https://github.com/transmission/transmission/pull/1787
<jirc> http://beam-wisdoms.clau.se/en/latest/index.html
<jirc> https://www.bryanbraun.com/checkboxland/
<jirc> https://fmentzer.github.io/posts/2020/dictionary/
<jirc> https://torrentfreak.com/the-worlds-oldest-active-torrent-turns-18-210912/
<jirc> https://briancallahan.net/blog/20210911.html
<jirc> https://electroagenda.com/en/a-summary-of-electronics/
<jirc> https://tribunemag.co.uk/2021/09/fossil-fuel-capitalism-is-cutting-our-lives-short
<jirc> https://physicsbaseddeeplearning.org/intro.html
<jirc> http://c-faq.com/
<jirc> https://github.com/amule-project/amule/releases/tag/2.3.3
<jirc> https://gist.github.com/rain-1/c4be54e6506116c7b99e8f474a3b1ca8
<jirc> https://www.ryanliptak.com/blog/code-coverage-zig-callgrind/
<jirc> https://blog.tygr.info/emacs/org-real.html
<jirc> https://www.techspot.com/news/91174-germany-federal-police-agency-secretly-purchased-used-controversial.html
<jirc> https://peermaps.org/
<jirc> https://gravityandlevity.wordpress.com/2009/04/08/why-cant-i-go-faster-than-the-speed-of-light-hints-from-electrodynamics/
<jirc> https://unit42.paloaltonetworks.com/azure-container-instances/
<jirc> https://freebibleapi.com
<jirc> https://yarchive.net/
<jirc> https://bitbucket.org/blaze-lib/blaze/src/master/
<jirc> https://cacm.acm.org/magazines/2021/9/255040-the-future-is-big-graphs/fulltext
<jirc> https://cloudintosh.sharedigm.com
<jirc> https://humungus.tedunangst.com/r/honk
<jirc> https://www.johndcook.com/blog/2021/09/11/much-less-than-much-greater-than/
```

### ,convert 

converts an amount from a unit to a unit

Usage:

```shell
,convert <amount> <from> <to>
```

Example:

```text
<foo> ,convert 1 kg punds
<jirc> punds not valid unit
<foo> ,convert 1 kg lbs
<jirc> 1 kilograms is 2.204623 pounds
<foo> ,convert 1 mile km
<jirc> 1 miles is 1.609344 kilometers
```

### ,roll

roll an N sided die

Usage:
```text
,roll <int>
```

Example:

```text
<foo> ,roll help
<jirc> http://localhost:8000/#roll
<foo> ,roll 1000
<jirc> 320
<foo> ,roll 9223372036854775809
<jirc> Invalid input 9223372036854775809
```

### ,choose

choose a random word from a list of words

Usage:
```text
,choose [<word>, <word>, ...]
```
Example:

```text
<foo> ,choose one two three
<jirc> two
<foo> ,choose one two three
<jirc> one
```

### ,sha256

hex encode sha256 sum of a string

Usage:
```text
,sha256 ...args
```

Example:
```text
<foo> ,sha256 some string to hash
<jirc> ea83a45637a9af470a994d2c9722273ef07d47aec0660a1d10afe6e9586801ac
```