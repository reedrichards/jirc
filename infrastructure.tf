terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.20.0"
    }
  }

  required_version = "~> 1.0"
  backend "http" {

  }
}

variable "aws_region" {
  description = "region to create aws app runner service & ecr repo"
  default = "us-east-1"
}

variable "image_tag"  {
  description = "image tag to be deployed to aws app runner"
  default = "latest"
}

variable "JIRC_PASS" {
  description = "password for jirc irc bot"
}

provider "aws" {
  region = var.aws_region
}


resource "aws_ecr_repository" "jirc" {
  name                 = "jirc"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_apprunner_service" "jirc" {
  service_name = "jirc"

  source_configuration {
    authentication_configuration {
	  access_role_arn = "arn:aws:iam::150301572911:role/service-role/AppRunnerECRAccessRole"
	}
    image_repository {
      image_configuration {
        port = "8000"
        runtime_environment_variables = {
          JIRC_NICK = "jirc"
          JIRC_SERVER = "smelly.irc.jjk.is:6697"
          JIRC_PASS = var.JIRC_PASS
          JIRC_CHANS = "jirc,n"
          JIRC_URL = "https://jirc.jjk.is"
        }
      }
	  image_identifier      = "${aws_ecr_repository.jirc.repository_url}:${var.image_tag}"
      image_repository_type = "ECR"
    }
  }
}
resource "aws_apprunner_custom_domain_association" "jirc" {
  domain_name = "jirc.jjk.is"
  service_arn = aws_apprunner_service.jirc.arn
}
